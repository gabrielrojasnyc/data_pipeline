provider "aws" {
  region = "us-east-1"
}

locals {
  role_name        = "DataPipelineDefaultRole"
  policy_arn       = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforDataPipelineRole"
  group_name       = "DataPipelineDevelopers"
  policy_group_arn = "arn:aws:iam::aws:policy/AWSDataPipeline_FullAccess"
  pipeline_name    = "data-pipeline-test"
}

resource "aws_iam_role" "data_pipeline" {
  name               = local.role_name
  assume_role_policy = "${file("data_pipeline_policy.json")}"
}

module "aws_iam_role_policy_attachment" {
  source           = "../modules/iam/"
  role_name        = local.role_name
  policy_arn       = local.policy_arn
  group_name       = local.group_name
  policy_group_arn = local.policy_group_arn
}

module "aws_data_pipelines" {
  source        = "../modules/data_pipeline"
  pipeline_name = local.pipeline_name
}
