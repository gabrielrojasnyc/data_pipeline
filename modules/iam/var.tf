variable "role_name" {
    description = "Pass name of the role"
    type = string
}

variable "policy_arn" {
    description = "Attach existing AWS policy to role"
    type = string
}

variable "group_name" {
    description = "To create blank IAM group"
    type = string
}

variable "policy_group_arn" {
    description = "Policy to attach to a IAM group"
    type = string
}
