resource "aws_iam_role_policy_attachment" "attach_policy" {
    role = "${var.role_name}"
    policy_arn = "${var.policy_arn}"
}
resource "aws_iam_group_policy_attachment" "attach_policy" {
    group = "${var.group_name}"
    policy_arn = "${var.policy_group_arn}"
    depends_on = [aws_iam_group.group_developers]
}

resource "aws_iam_group" "group_developers" {
    name = "${var.group_name}"
}
