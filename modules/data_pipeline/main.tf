resource "aws_datapipeline_pipeline" "data_pipelime" {
    name = "${var.pipeline_name}"
    
    provisioner "local-exec" {
        command = "aws datapipeline describe-pipelines --pipeline-ids df-xxxxx"
    }
}